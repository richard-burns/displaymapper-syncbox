#include <Keyboard.h>
#include <SimpleTimer.h>

const int buttonPin = 9;

// this sets the key to press
const char interactive = '0';

// this sets how often to send the command
long interval = 86400000;
int t;

SimpleTimer timer;

void setup() {
  // put your setup code here, to run once:
   // Keyboard.begin()0
   Serial.begin(9600);
   pinMode(buttonPin, INPUT_PULLUP);
   t = timer.setInterval(interval, triggerKey);

}

void triggerKey() {
     Keyboard.write(interactive);
     Serial.println(interactive);
}

void loop() {
  int buttonState = digitalRead(buttonPin);

    String messagein = Serial.readString();

    if (messagein.length() > 4) {
        char key = messagein.charAt(4);
        Keyboard.write(key);  
        Serial.println(interactive);
        timer.restartTimer(t);
    }

    if (buttonState == 0) {
       Keyboard.write(interactive);  
       Serial.println(interactive);
       timer.restartTimer(t);
    }

  //  Keyboard.write('0');
   // delay(1092000);
   timer.run();
  
} //00000

//0000iii000000000000
